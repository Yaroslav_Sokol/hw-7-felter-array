// 1. Це функція яка дозволяє виконати певну операцію для кожного елементу в масиві ьез необхідності створювати цикл.
// 2. Можна присвоїти порожній масив, тобто "null", або використати метод "splice()" або "length()" та задать їм 0.
// 3. Використати метод .isArray(), якщо це масив то повернеться "true", якщо ні то "false".

function filterBy(array, type) {
    return array.filter(item => typeof item !== type);
}

const array = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(array, 'string');
console.log(filteredArray);